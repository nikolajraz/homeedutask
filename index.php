<?php
include('classes/DVD.php');
include('classes/Book.php');
include('classes/Furniture.php');

?>

<!DOCTYPE html>
<html>
<head>
    <title>Product List</title>
    <link href="Styles/styles.css"
          rel="stylesheet"
          type="text/css">
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="Script/javascript.js"></script>
</head>
<body>

<div id="setup">
    <form method="POST" action="Delete.php">
        <div id="header">
            Product list
            <button type="button" onclick="toAnotherPage('GET','add.html')" class="change">Add page</button>
            <input type="submit" name="delete" class="but" id="deletebut" value="Save" onclick="areYouSure();">
            <select id="deletesel" onchange="toggle(value);">
                <option value="deletefew">Delete all selected elements</option>
                <option value="deleteall">Mass delete action</option>
            </select>
        </div>

        <div id="main">
            <?php
            $pdo = Database::connect();
            $query = Database::query($pdo);
            ?>
            <div class="row">
                <?php
                while (($round = $query->fetch(PDO::FETCH_OBJ))) {
                    if ($round->Type == 1) {
                        $dvd = new DVD();
                        $dvd->setSku($round->SKU);
                        $dvd->setName($round->Name);
                        $dvd->setPrice($round->Price);
                        $dvd->setSize($round->Additional);
                        echo '<div class="icon">'.$dvd->getSku().'<br>'.$dvd->getName().'<br>'.$dvd->getPrice().'$ <br>'.$dvd->getSize().'MB <br>'.'<input type="checkbox" name="checkbox[]" value="'.$dvd->getSku().'" class="cbox"></div>';
                    }
                }
                ?>
            </div>
            <div class="row">
                <?php
                $query = Database::query($pdo);
                while (($round = $query->fetch(PDO::FETCH_OBJ))) {
                    if ($round->Type == 2) {
                        $book = new Book();
                        $book->setSku($round->SKU);
                        $book->setName($round->Name);
                        $book->setPrice($round->Price);
                        $book->setWeight($round->Additional);
                        echo '<div class="icon">'.$book->getSku().'<br>'.$book->getName().'<br>'.$book->getPrice().'$ <br>'.$book->getWeight().'KG <br>'.'<input type="checkbox" name="checkbox[]" value="'.$book->getSku().'" class="cbox"></div>';
                    }
                }
                ?>
            </div>
            <div class="row">
                <?php
                $query = Database::query($pdo);
                while (($round = $query->fetch(PDO::FETCH_OBJ))) {
                    if ($round->Type == 3) {
                        $furniture = new Furniture();
                        $furniture->setSku($round->SKU);
                        $furniture->setName($round->Name);
                        $furniture->setPrice($round->Price);
                        $furniture->setDimension($round->Additional);
                        echo '<div class="icon">'.$furniture->getSku().'<br>'.$furniture->getName().'<br>'.$furniture->getPrice().'$ <br>'.$furniture->getDimension().'<br>'.'<input type="checkbox" name="checkbox[]" value="'.$furniture->getSku().'" class="cbox"></div>';
                    }
                }
                ?>
            </div>
        </div>
    </form>
</div>
</body>
</html>