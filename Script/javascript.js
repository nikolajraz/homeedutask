/**
 * @param string $i
 *
 * @return nothing
 * @toggle all checkboxes if is selected "Mass delete action", or clear all checkboxes if is selected "Delete all selected elements"
 */
function toggle($i) {
    if ($i == 'deleteall') {
        var checkboxes = document.getElementsByClassName('cbox');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = true;
        }
    } else if ($i == 'deletefew') {
        var checkboxes = document.getElementsByClassName('cbox');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = false;
        }
    }
}

/**
 * @param none
 *
 * @return nothing
 * @ask user if he is sure to make delete action
 */
function areYouSure() {
    var ans = confirm("Are you sure to delete selected items?");
    if (ans == false)
        location.href = this.href;
}

/**
 * @param string $i
 *
 * @return nothing
 * @show hidden blocks according to selected type switcher
 */
function changeFunc($i) {
    var size = $('.size');
    var weight = $('.weight');
    var dimensions = $('.dimensions');
    size.hide();
    weight.hide();
    dimensions.hide();
    switch ($i) {
        case 'DVD': {
            size.show();
            document.getElementsByName("Size")[0].setAttribute("data-flag", "1");
            document.getElementsByName("Weight")[0].setAttribute("data-flag", "0");
            document.getElementsByName("Height")[0].setAttribute("data-flag", "0");
            document.getElementsByName("Width")[0].setAttribute("data-flag", "0");
            document.getElementsByName("Length")[0].setAttribute("data-flag", "0");
        }
            break;
        case 'Book': {
            weight.show();
            document.getElementsByName("Size")[0].setAttribute("data-flag", "0");
            document.getElementsByName("Weight")[0].setAttribute("data-flag", "1");
            document.getElementsByName("Height")[0].setAttribute("data-flag", "0");
            document.getElementsByName("Width")[0].setAttribute("data-flag", "0");
            document.getElementsByName("Length")[0].setAttribute("data-flag", "0");
        }
            break;
        case 'Furniture': {
            dimensions.show();
            document.getElementsByName("Size")[0].setAttribute("data-flag", "0");
            document.getElementsByName("Weight")[0].setAttribute("data-flag", "0");
            document.getElementsByName("Height")[0].setAttribute("data-flag", "1");
            document.getElementsByName("Width")[0].setAttribute("data-flag", "1");
            document.getElementsByName("Length")[0].setAttribute("data-flag", "1");
        }
            break;
    }
}

/**
 * @param object $i
 *
 * @return true or false
 * @check if the object value is empty string
 */
function checkEmpty($i) {
    return $i.value == "";
}

/**
 * @param none
 *
 * @return nothing
 * @check if the object value contain only numbers or is empty
 */
function checkNumber() {
    if (allExceptNumbers(this.value) || checkEmpty(this)) {
        makeAlert(this);
    } else {
        makeNormal(this);
    }
}

/**
 * @param none
 *
 * @return nothing
 * @check if the object value contain only numbers, points or commas, or is empty
 */
function checkFloat() {
    if (allExceptFloat(this.value) || checkEmpty(this)) {
        makeAlert(this);
    } else {
        makeNormal(this);
    }
}

/**
 * @param none
 *
 * @return nothing
 * @check if the object value length is less than 11 or empty
 */
function checkSKU() {
    if (this.value.length > 9 || checkEmpty(this)) {
        makeAlert(this);
    } else {
        makeNormal(this);
    }
}

/**
 * @param none
 *
 * @return nothing
 * @check name for the emptiness
 */
function checkName() {
    if (checkEmpty(this)) {
        makeAlert(this);
    } else {
        makeNormal(this);
    }
}

/**
 * @param $i
 *
 * @return nothing
 * @make field red color that signals to user to his mistakes and set attribute "data-correct" to "0" value
 */
function makeAlert($i) {
    $i.style.borderColor = "red";
    $i.style.backgroundColor = "#F0A5AC";
    $i.setAttribute("data-correct", "0");
}

/**
 * @param object $i
 *
 * @return nothing
 * @make field normal color and set attribute "data-correct" to "1" value
 */
function makeNormal($i) {
    $i.style.borderColor = "darkorange";
    $i.style.backgroundColor = "white";
    $i.setAttribute("data-correct", "1");
}

/**
 * @param string $i
 *
 * @return true or false
 * @check if the string contain something except numbers
 */
function allExceptNumbers($i) {
    return !/^[0-9]+$/.test($i);
}

/**
 * @param string $i
 *
 * @return true or false
 * @check if the string contain something except numbers, points or commas
 */
function allExceptFloat($i) {
    return !/^[0-9.,]+$/.test($i);
}

/**
 * @param none
 *
 * @return false or nothing
 * @check every field of form for mistakes or emptiness, if there is no mistakes submit form
 */
function checkFields() {
    var inputs = document.getElementsByClassName('inp');
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].getAttribute("data-flag") == "1" && inputs[i].value == "") {
            makeAlert(inputs[i]);
            alert("Please, fill all required fields");
            return false;
        } else if (inputs[i].getAttribute("data-correct") == "0") {
            alert("Please, fill fields correctly!");
            return false;
        }
    }
    document.getElementById("subform").submit();
}

/**
 * @param strings $i,$i
 *
 * @return nothing
 * @load another page content in block setup
 */
function toAnotherPage($i, $j) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4 && xhttp.status === 200)
            document.getElementById("setup").innerHTML = this.responseText;
    }
    xhttp.open($i, $j, true);
    xhttp.send();
}

/**
 * @param none
 *
 * @return nothing
 * @subscribe elements for events looking at their "data-type" attribute
 */
function trigger() {
    var inputs = document.getElementsByClassName('inp');
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].getAttribute("data-type") == "Number")
            inputs[i].addEventListener("change", checkNumber);
        if (inputs[i].getAttribute("data-type") == "Float")
            inputs[i].addEventListener("change", checkFloat);
        if (inputs[i].getAttribute("data-type") == "SKU")
            inputs[i].addEventListener("change", checkSKU);
        if (inputs[i].getAttribute("data-type") == "Name")
            inputs[i].addEventListener("change", checkName);
    }
}