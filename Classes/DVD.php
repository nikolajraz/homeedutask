<?php

include('Product.php');
include('Database.php');

class DVD extends Product
{
    private $size;

    public function getType()
    {
        return 1;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    protected function AddAdditional()
    {
        return $this->getSize();
    }

}