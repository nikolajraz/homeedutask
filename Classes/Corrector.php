<?php

class Corrector
{
    /**
     * @param string $j
     *
     * @return string $i
     * @delete all dangerous symbols from received data
     */
    public static function allExceptNumLet($j)
    {
        $i = preg_replace("/[^a-z0-9]/i", "", $_POST[$j]);;

        return $i;
    }

    /**
     * @param string $j
     *
     * @return string $i
     * @delete all dangerous or wrong symbols from received data
     */
    public static function allExceptNum($j)
    {
        $i = preg_replace("/[^0-9]/i", "", $_POST[$j]);;

        return $i;
    }

    /**
     * @param string $j
     *
     * @return string $i
     * @delete all dangerous or wrong symbols from received data and replace , in float numbers on .
     */
    public static function allExceptFloat($j)
    {
        $i = preg_replace("/[,]/", ".", $_POST[$j]);;
        $i = preg_replace("/[^0-9.]/i", "", $i);;

        return $i;
    }

}