<?php

class Database
{
    /**
     * @param none
     *
     * @return exemplar of class PDO
     * @connect with MySQL DataBase using class PDO
     */
    public static function connect()
    {
        $connection = 'mysql:host=127.0.0.1;dbname=edu_task';

        return new PDO($connection, 'root', '');
    }

    /**
     * @param $cbox , $pdo
     *
     * @return nothing
     * @delete selected object
     */
    public static function delete($cbox, $pdo)
    {
        $sql = $pdo->prepare("DELETE FROM `products` WHERE `sku` = :sku");
        $sql->bindParam(':sku', $cbox);
        $sql->execute();
    }

    /**
     * @param $pdo
     *
     * @return $query
     * @get everything from product table
     */
    public static function query($pdo)
    {
        $query = $pdo->query('SELECT * FROM `products`');

        return $query;
    }
}

