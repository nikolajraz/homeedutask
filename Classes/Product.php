<?php

class Product
{
    protected $sku;
    protected $name;
    protected $price;

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getType()
    {
        return null;
    }

    /**
     * @param none
     *
     * @return nothing
     * @add new product
     */
    public function Add()
    {
        $pdo = Database::connect();
        $sql = 'INSERT INTO products(SKU, Name, Price, Type, Additional) VALUES(:sku,:name,:price,:type, :additional)';
        $query = $pdo->prepare($sql);
        $query->execute([
            'sku' => $this->getSku(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'type' => $this->getType(),
            'additional' => $this->AddAdditional(),

        ]);
    }

    protected function AddAdditional()
    {
        return null;
    }
}