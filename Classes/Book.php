<?php

class Book extends Product
{
    private $weight;

    public function getType()
    {
        return 2;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    protected function AddAdditional()
    {
        return $this->getWeight();
    }
}