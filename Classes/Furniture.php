<?php


class Furniture extends Product
{
    private $dimension;

    public function getType()
    {
        return 3;
    }

    public function getDimension()
    {
        return $this->dimension;
    }

    public function setDimension($dimension)
    {
        $this->dimension = $dimension;
    }

    protected function AddAdditional()
    {
        return $this->getDimension();
    }
}
