<?php

include('classes/DVD.php');
include('classes/Book.php');
include('classes/Furniture.php');
include('classes/Corrector.php');

$flag = 0;
if ($_POST['Size'] != 0) {
    $flag = 1;
} else {
    if ($_POST['Weight'] != 0) {
        $flag = 2;
    } else {
        if ($_POST['Height'] != 0) {
            $flag = 3;
        }
    }
}

switch ($flag) {
    case 1:
        {
            $dvd = new DVD();
            $dvd->setSku(Corrector::allExceptNumLet('SKU'));
            $dvd->setName(Corrector::allExceptNumLet('Name'));
            $dvd->setPrice(Corrector::allExceptNum('Price'));
            $dvd->setSize(Corrector::allExceptNum('Size'));
            $dvd->Add();
        }
        break;
    case 2:
        {
            $book = new Book();
            $book->setSku(Corrector::allExceptNumLet('SKU'));
            $book->setName(Corrector::allExceptNumLet('Name'));
            $book->setPrice(Corrector::allExceptNum('Price'));
            $book->setWeight(Corrector::allExceptFloat('Weight'));
            $book->Add();
        }
        break;
    case 3:
        {
            $furniture = new Furniture();
            $furniture->setSku(Corrector::allExceptNumLet('SKU'));
            $furniture->setName(Corrector::allExceptNumLet('Name'));
            $furniture->setPrice(Corrector::allExceptNum('Price'));
            $furniture->setDimension(Corrector::allExceptNum('Height').'X'.Corrector::allExceptNum('Length').'X'.Corrector::allExceptNum('Width'));
            $furniture->Add();
        }
        break;
}

header('Location: /');
