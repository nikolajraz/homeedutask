-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 07, 2020 at 10:08 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edu_task`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `SKU` varchar(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Price` int(11) UNSIGNED NOT NULL,
  `Type` int(11) UNSIGNED NOT NULL,
  `Additional` varchar(255) NOT NULL,
  KEY `Type` (`Type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`SKU`, `Name`, `Price`, `Type`, `Additional`) VALUES
('DV0000001', 'DVD1', 5, 1, '500'),
('DV0000002', 'DVD2', 10, 1, '1000'),
('DV0000003', 'DVD3', 8, 1, '800'),
('BS0000001', 'Book1', 15, 2, '4.5'),
('BS0000002', 'Book2', 17, 2, '4.7'),
('BS0000003', 'Book3', 7, 2, '5.7'),
('FT0000003', 'Furniture3', 150, 3, '100X100X100'),
('FT0000001', 'Furniture1', 80, 3, '50X50X50'),
('FT0000002', 'Furniture2', 100, 3, '70X90X80');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `Name` varchar(255) NOT NULL,
  `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`Name`, `Id`) VALUES
('DVD', 1),
('Book', 2),
('Furniture', 3);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`Type`) REFERENCES `types` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
